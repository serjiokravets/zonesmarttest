import {countSfx} from "../utils/common";

export class TableFilter {
    constructor(limit = 10) {
        this.limit = 10
        this.offset = 0
        this.search = ""
        this.count = 0
    }

    clear() {
        this.offset = 0
    }
}

export class Order {
    constructor(order) {
        this.order_id = order.order_id
        this.create_date = new Date(order.create_date).toLocaleDateString()
        this.items = order.items
        this.items_label = `${ order.items.length } ${ countSfx(order.items.length, ['товар', 'товара', 'товаров']) }`
        this.is_paid = order.is_paid
        this.is_shipped = order.is_shipped
        this.is_completed = order.is_completed
        this.buyer = order.buyer
        this.total_price = "$" + order.total_price.toFixed(0)
        this.is_items_shown = false
    }
}
