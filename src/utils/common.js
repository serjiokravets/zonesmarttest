export function throttle (callback, limit) {
    let wait = false;
    return function () {
        if (!wait) {
            callback.call();
            wait = true;
            setTimeout(function () {
                wait = false;
            }, limit);
        }
    }
}

export function debounce (fn, delay) {
    let timeoutID = null
    return function () {
        clearTimeout(timeoutID)
        let args = arguments
        let that = this
        timeoutID = setTimeout(function () {
            fn.apply(that, args)
        }, delay)
    }
}

export function countSfx(value, words) {
    value = Math.abs(value) % 100;
    let num = value % 10;

    if(value > 10 && value < 20) return words[2];

    if(num > 1 && num < 5) return words[1];

    if(num === 1) return words[0];

    return words[2];
}
