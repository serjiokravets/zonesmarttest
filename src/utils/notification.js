export function Notification(message, type, duration = 3000) {
    alert(`${type}: ${message}`)
}

export class Notificate {
    static error(message) {
        return Notification(message, "Error")
    }
    static success(message) {
        return Notification(message, "Success")
    }
}
