import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'orders',
        component: () => import(/* webpackChunkName: "about" */ '../views/Dash.vue'),
        meta: {
            protected: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue'),

    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.meta.protected && !localStorage.getItem('access')) {
        next({ name: "login" })
    }
    else {
        next()
    }
})

export default router
