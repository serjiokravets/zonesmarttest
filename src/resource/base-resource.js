import http from './http'
import {Notificate} from "../utils/notification";

export class BaseResource {
    static async tryGet (url, params, config = {}, is_public = false, errorHandling = BaseResource.handleError) {
        try {
            if (params) config.params = params
            return await http(is_public).get(url, config).then(response => response.data)
        } catch (e) {
            return errorHandling(e)
        }
    }

    static async tryPost (url, request, config = {}, is_public = false, errorHandling = BaseResource.handleError) {
        try {
            return await http(is_public).post(url, request, config).then(response => response.data)
        } catch (e) {
            return errorHandling(e)
        }
    }

    static handleError (error) {
        const status = error?.response?.status
        if (status === 401 || status === 400) {
            console.log(error.response)
            Notificate.error(error.response.data.detail)
        }

        return Promise.reject(error)
    }
}
