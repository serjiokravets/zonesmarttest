import { BaseResource } from './base-resource'
import store from "../store";

export default class AuthResource {
    static async login (request) {
        return await BaseResource.tryPost(auth_urls.create, request, undefined, true)
    }

    static async refresh () {
        if (AuthResource.refresh_instance) return AuthResource.refresh_instance

        let instance = BaseResource.tryPost(auth_urls.refresh, {
            refresh: localStorage.refresh
        }, undefined, true)

        instance.then(data => {
            localStorage.access = data.access
            return Promise.resolve()
        })

        instance.catch(e => {
            store.dispatch("logout")
            return Promise.reject(e)
        })

        instance.finally(() => {
            AuthResource.refresh_instance = null
        })

        return AuthResource.refresh_instance = instance
    }
}

const auth_urls = {
    create: 'auth/jwt/create/',
    refresh: 'auth/jwt/refresh/'
}
