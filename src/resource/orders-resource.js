import { BaseResource } from './base-resource'

export default class OrdersResource {
    static async list (request) {
        return await BaseResource.tryGet(orders_urls.list, request, undefined)
    }
}

const orders_urls = {
    list: 'zonesmart/order/'
}
