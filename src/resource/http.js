import axios from 'axios'
import AuthResource from "./auth-resource";

export const base_url = "https://zonesmart.su/api/v1/"

const __public_instance = createInstance(undefined, undefined, onResponseSuccess, error => Promise.reject(error))
const __instance = createInstance(onRequest, onRequestError, onResponseSuccess, onResponseError)

function onRequest(request_config) {
    request_config.headers.Authorization = `JWT ${localStorage.access}`

    return request_config
}
function onRequestError(error) { return error }
function onResponseSuccess(response) { return response }
async function onResponseError(error) {
    if (error?.response?.status === 401) {
        const original_request = error.config
        return await AuthResource.refresh().then(() => {
            original_request.headers.Authorization = `JWT ${localStorage.getItem("access")}`;
            return __instance()(original_request)
        }).catch(e => Promise.reject(e))
    }
    return Promise.reject(error)
}

// я бы не разделял инстансы если бы бек отдавал 403 на протухший токен

function createInstance(onRequest, onRequestError, onResponseSuccess, onResponseError) {
    let __instance = null

    return function () {
        if (__instance) return __instance

        const instance = axios.create({
            baseURL: base_url
        })

        instance.interceptors.request.use(onRequest, onRequestError)
        instance.interceptors.response.use(onResponseSuccess, onResponseError)

        return __instance = instance
    }
}

export default function http (is_public = false) {
    if (is_public) return __public_instance()

    return __instance()
}
