import auth from "./auth"
import btn from "./btn"
import field from "./field"
import check from "./check"
import tbl from "./tbl/tbl"
import tblA from "./tbl/tbl-a"
import tblTd from "./tbl/tbl-td"
import tblBool from "./tbl/tbl-bool"
import tblToggle from "./tbl/tbl-toggler"
import transitionExpand from "./transition-expand"

export function install(Vue) {
    Vue.component(auth.name, auth)
    Vue.component(btn.name, btn)
    Vue.component(field.name, field)
    Vue.component(check.name, check)
    Vue.component(tbl.name, tbl)
    Vue.component(tblA.name, tblA)
    Vue.component(tblTd.name, tblTd)
    Vue.component(tblBool.name, tblBool)
    Vue.component(tblToggle.name, tblToggle)
    Vue.component(transitionExpand.name, transitionExpand)
}
