import router from '../router'
import AuthResource from '../resource/auth-resource'
class AccountState {
    constructor() {
        if (localStorage.access) this.account = { name: 'Serjio', lastName: 'Kravets' }
    }
}
export const AuthModule = {
    state: new AccountState(),
    getters: {
        full_name: (state) => state.account ? `Made by ${state.account.name} ${state.account.lastName}` : 'Анон'
    },
    mutations: {
        LOGIN: (state, payload) => {
            // в пейлоад можно класть раскодированный токен и сохранять в стейт
            state.account = { name: 'Serjio', lastName: 'Kravets' }
            localStorage.access = payload.access
            localStorage.refresh = payload.refresh
        },
        LOGOUT: (state) => {
            state.account = null
            localStorage.removeItem("access")
            localStorage.removeItem("refresh")
        }
    },
    actions: {
        async login (context, payload) {
            try {
                await AuthResource.login(payload).then(data => {
                    context.commit('LOGIN', data)
                    router.push({ name: 'orders' })
                })
            }
            catch (e) {
                return e
            }
        },
        async logout (context) {
            context.commit('LOGOUT')
            await router.push({ name: 'login' })
        }
    }
}
