module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: [
        'plugin:vue/essential',
    ],
    parserOptions: {
        parser: 'babel-eslint'
    },
    rules: {
        'no-console': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'camelcase': 'off',
        'no-return-assign': 'off',
        'space-before-function-parent': 'off',
        "indent": ["error", 4],
        "vue/no-dupe-keys": "off",
        "vue/return-in-computed-property": "off"
    }
}
